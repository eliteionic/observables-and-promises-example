import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { from, forkJoin } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(public dataService: DataService) {}

  async loadTheThingsSlow() {
    console.time('requests');

    const thingOne = await this.dataService.getThingOne();
    console.log('thing one loaded');

    const thingTwo = await this.dataService.getThingTwo();
    console.log('thing two loaded');

    const thingThree = await this.dataService.getThingThree();
    console.log('thing three loaded');
    console.timeEnd('requests');

    console.log('All done!');
    console.log(thingOne);
    console.log(thingTwo);
    console.log(thingThree);
  }

  async loadTheThingsQuick() {
    console.time('requests');

    const promiseOne = this.dataService.getThingOne();
    const promiseTwo = this.dataService.getThingTwo();
    const promiseThree = this.dataService.getThingThree();

    const values = await Promise.all([promiseOne, promiseTwo, promiseThree]);

    console.timeEnd('requests');
    console.log('thingOne: ', values[0]);
    console.log('thingTwo: ', values[1]);
    console.log('thingThree: ', values[2]);
  }

  loadObservables() {
    const observableOne = from(this.dataService.getThingOne());
    const observableTwo = from(this.dataService.getThingTwo());
    const observableThree = from(this.dataService.getThingThree());

    forkJoin([observableOne, observableTwo, observableThree]).subscribe(
      (values) => {
        console.log('thingOne: ', values[0]);
        console.log('thingTwo: ', values[1]);
        console.log('thingThree: ', values[2]);
      }
    );
  }
}
